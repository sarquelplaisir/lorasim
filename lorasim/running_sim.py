import os
from time import time


# DEFAULT PARAMETERS
FILENAME = "loraDirMulBS.py"
NODES = 10
AVGSEND = 600000	# 10 minutes
EXPERIMENT = [9, 10]
SIMTIME = 3600000	# 1 hour
BASESTATIONS = 1
COLLISION = 1
ADR = False
PER_MOBILE_NODES = 0
CAPTURE_EFFECT = True

# Create the command to execute to run a simulation
def create_cmd(param):
	
	cmd = "python3 "+FILENAME
	
	# Pour utiliser la version de base de loraDirMulBS.py, qui ne prend que 5 arguments
	for i in range(8):
		p = str(param[i])
		cmd += " "+p
	
	# Pour la version "avancée" de loraDirMulBS.py, avec + d'arguments (adr, nb de noeuds mobiles etc.)
	#for i in range(len(param)):
		#p = str(param[i])
		#cmd += " "+p
	
	return cmd


# SIMULATION 1
# Impact of the number of nodes
def sim_nbNodes(adr, per_mobile_nodes):

	print("\n========== Starting simulation 1 - Number of nodes ==========")
	if adr:
		print("ADR activated")
	else:
		print("No ADR")
	if per_mobile_nodes!=0:
		print("Mobile nodes percentage :", per_mobile_nodes*100,"%")
	else:
		print("No mobile nodes")
	
	exp1_t = time()

	val_nodes = []
	# Linear scale
	for i in range(0,2200,200):
		val_nodes.append(i)
	# "Log" scale
	#val_nodes = [0,50,100,150,200,300,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000]
	print("Number of nodes range :",val_nodes)

	# Real test
	for exp in EXPERIMENT:
		for n in val_nodes:
			print(per_mobile_nodes)
			param = [n,AVGSEND,exp,SIMTIME,BASESTATIONS,COLLISION, per_mobile_nodes, adr]
			cmd = create_cmd(param)
			print(cmd,"\n")
			os.system(cmd)

	# Use for debug
	#for n in range(1,2,1):
		#param = [n,AVGSEND,EXPERIMENT,SIMTIME,BASESTATIONS,COLLISION,adr,per_mobile_nodes]
		#cmd = create_cmd(param)
		#print(cmd,"\n")
		#os.system(cmd)
	
	print("\nSimulation 1 execution time :", time()-exp1_t)


# SIMULATION 2
# Impact of the number of gateways
def sim_nbGtw(adr, per_mobile_nodes):
	
	print("\n========== Starting simulation 2 - Number of gateways ==========\n")
	if adr:
		print("ADR activated")
	else:
		print("No ADR")
	if per_mobile_nodes!=0:
		print("Mobile nodes percentage :", per_mobile_nodes*100,"%")
	else:
		print("No mobile nodes")
	
	exp2_t = time()

	val_gtw = [1,2,3,4,6,8,24]
	print("Number of gateways range :",val_gtw)
	
	# Real test
	#for g in val_gtw:
		#param = [NODES,AVGSEND,EXPERIMENT,SIMTIME,g,COLLISION,adr,per_mobile_nodes]
		#cmd = create_cmd(param)
		#print(cmd,`"\n")
		#os.system(cmd)
	# Use for debug
	for g in range(1,2,1):
		param = [NODES,AVGSEND,EXPERIMENT,SIMTIME,g,COLLISION,per_mobile_nodes, adr]
		cmd = create_cmd(param)
		print(cmd,"\n")
		os.system(cmd)
	
	print("\nSimulation 2 execution time :", time()-exp2_t)


if __name__ == '__main__':
	
	global_start_t = time()

	nb_rep = 1
	#mobile_nodes_range = [0,0.1,0.2]
	# Without mobile nodes
	mobile_nodes_range = [0]
	
	# PRIORITE 1 : Performances en fonction du nombre de noeuds
	# 1.Pas d'ADR ni de blind ADR
	for i in range(nb_rep):
		for m in mobile_nodes_range:
			sim_nbNodes(False,m)
	# 2.ADR et blind ADR activés
	#for i in range(nb_rep):
		#for m in mobile_nodes_range:
			#sim_nbNodes(True,m)

	# PRIORITE 2 : Performances en fonction du nombre de gateways
	# 1.Pas d'ADR ni de blind ADR
	#for i in range(nb_rep):
		#for m in mobile_nodes_range:
			#sim_nbGtw(False,m)
	# 2.ADR et blind ADR activés
	#for i in range(nb_rep):
		#for m in mobile_nodes_range:
			#sim_nbGtw(True,m)
	
	# PRIORITE 3 : Performances en fonction de la fréquence, puissance et effet capture
	# 1. Bande de fréquence (433 vs 868 MHz)
	# 2. Puissance (Pmax vs Pmax/2)
	# 3. Activation ou non de l'effet capture

	print("\nTotal execution time :", time()-global_start_t)
