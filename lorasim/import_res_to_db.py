import psycopg2
import sys
import os


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def clean_file_int(file, rm):
    with open(file, 'r') as f:
        filedata = f.read()

    filedata = filedata.replace('.0\t', '\t')

    name = file.split('.')
    name.append(name[1])
    name[1] = "_cleaned."
    name = "".join(name)
    with open(name, 'w') as f:
        f.write(filedata)
    if rm:
        os.remove(file)


def import_experiment(db, file='res/experiments.dat', rm=False):
    print('Importing Experiment')
    with db.cursor() as cur:
        with open(file, 'r') as f:
            lines = f.readlines()
            ll = len(lines)
            for i, l in enumerate(lines):
                l = l.strip('\n')
                data = l.split('\t')
                if i == 0:
                    print('Headers :', l.replace('#', ''))
                    continue
                try:
                    cur.execute('INSERT INTO public."Experiment" VALUES (%s, %s, %s, %s, %s, %s, %s)',
                                (data[0], data[1], data[2], data[3], data[4], data[5], data[6]))
                    printProgressBar(i, ll, "Experiment Import")
                except psycopg2.errors.UniqueViolation as err:
                    db.rollback()
                    print("Fail :", data)
                    print(err)
                    return
    db.commit()
    if rm:
        os.remove(file)


def import_node(db, rm, file='res/nodes.dat'):
    print('Importing Node')
    with db.cursor() as cur:
        with open(file, 'r') as f:
            try:
                sql_copy = """
                            COPY public."Node" FROM stdin CSV header
                            DELIMITER '\t';
                            """
                cur.copy_expert(sql=sql_copy, file=f)
                if rm:
                    os.remove(file)
            except psycopg2.errors.UniqueViolation as err:
                db.rollback()
                print("Fail Node")
                print(err)
    db.commit()


def import_packet(db, rm, file='res/packets.dat'):
    print('Importing Packet')
    with db.cursor() as cur:
        f =  open(file, 'r')
        try:
            sql_copy = """
                                    COPY public."Packet" FROM stdin CSV header
                                    DELIMITER '\t';
                                    """
            cur.copy_expert(sql=sql_copy, file=f)
            if rm:
                os.remove(file)
        except psycopg2.errors.UniqueViolation as err:
            db.rollback()
            print("Fail Packet")
            print(err)
        db.commit()
        f.close()


def import_receivedby(db, file='res/receivedby.dat', rm=False):
    print('Importing ReceivedBy')
    with db.cursor() as cur:
        with open(file, 'r') as f:
            try:
                sql_copy = """
                                        COPY public."ReceivedBy" FROM stdin CSV header
                                        DELIMITER '\t';
                                        """
                cur.copy_expert(sql=sql_copy, file=f)
                if rm:
                    os.remove(file)
            except psycopg2.errors.UniqueViolation as err:
                db.rollback()
                print("Fail ReceivedBy")
                print(err)
    db.commit()


# TODO : lis ça https://www.dataquest.io/blog/loading-data-into-postgres/
if __name__ == '__main__':
    user = sys.argv[1]
    pwd = sys.argv[2]
    try:
        remove_file = sys.argv[3] == 'True'
    except IndexError:
        remove_file = True
    try:
        srv = sys.argv[4]
    except IndexError:
        srv = "ca.alban-prats.fr"
    with psycopg2.connect(host=srv, user=user, password=pwd,
                          dbname="lorasim") as conn:
        import_experiment(conn, rm=remove_file)
        import_node(conn, rm=remove_file)
        clean_file_int(file="res/packets.dat", rm=remove_file)
        import_packet(conn, file="res/packets_cleaned.dat", rm=remove_file)
        import_receivedby(conn, rm=remove_file)
